<?php

class CadastroController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
    	//Página do primeiro form de cadastro
    }

    public function proximoAction() 
    {
    	//Registra no BD e salva os dados do primeiro form.
    	//Depois redireciona para o próximo passo, já com o user logado.
    	Usuarios::cadProximo();

    }

}

