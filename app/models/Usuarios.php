<?php

use Phalcon\Validation;
use Phalcon\Mvc\Model\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;

class Usuarios extends \Phalcon\Mvc\Model
{

    public $id;

    public $nick;

    public $senha;

    public $email;

    public $nomecompleto;

    public $facebook;

    public $leaguenick;

    public $steam;

    public function validation()
    {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                    'message' => 'O campo email é obrigatório',
                )
            )
        );

        $this->validate(
            new PresenceOf(
                array(
                    'field' => 'nick',
                    'message' => 'O campo Nick é obrigatório',
                )
            )
        );

        $this->validate(
            new PresenceOf(
                array(
                    'field' => 'nome',
                    'message' => 'O campo Nome é obrigatório',
                )
            )
        );
        
        $this->validate(
            new PresenceOf(
                array(
                    'field' => 'sobrenome',
                    'message' => 'O campo Sobrenome é obrigatório',
                )
            )
        );
        
        $this->validate(
            new PresenceOf(
                array(
                    'field' => 'senha',
                    'message' => 'O campo Senha é obrigatório',
                )
            )
        );
        
        $this->validate(
            new PresenceOf(
                array(
                    'field' => 'senha2',
                    'message' => 'O campo Repita sua Senha é obrigatório',
                )
            )
        );

        $this->validate(
            new Confirmation(
                array(
                    'field' => 'senha',
                    'with' => 'senha2',
                    'message' => 'As senhas digitadas não correspondem',
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
            $message = $this->message;
        }
        return true;
    }

    public function getSource()
    {
        return 'usuarios';
    }

    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function cadProximo() {
        $usuarios = new Usuarios();
        if ($usuarios->validation){
            //Se passar pela validação do Form, registra o usuário
            $novouser = new Usuarios;
            $novouser->nick = $nick;
            $novouser->email = $email;
            $novouser->nome = $nome;
            $novouser->sobrenome = $sobrenome;
            $novouser->senha = $this->security->hash($senha);

            $novouser->create;

            echo "Usuário adicionado com sucesso!";
        }
        else {
            echo "msg de erro";
        }

    }

}
