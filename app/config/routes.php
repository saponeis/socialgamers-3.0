<?php

use Phalcon\Mvc\Router;

$router = new Router(false);


$router->add("/", [
    'controller' => 'index',
    'action' => 'index',
]);

$router->add("/login", [
    'controller' => 'login',
    'action' => 'login',
]);

return $router;