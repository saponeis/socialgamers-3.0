<h1>Cadastro</h1>

<?php echo $this->tag->form("cadastro/proximo"); ?>

 <p>
    <label for="nick">Nick</label>
    <?php echo $this->tag->textField("nick") ?>
 </p>

 <p>
    <label for="email">Email</label>
    <?php echo $this->tag->textField("email") ?>
 </p>

 <p>
    <label for="nome">Nome</label>
    <?php echo $this->tag->textField("nome") ?>
 </p>

 <p>
    <label for="sobrenome">Sobrenome</label>
    <?php echo $this->tag->textField("sobrenome") ?>
 </p>

 <p>
    <label for="senha">Senha</label>
    <?php echo $this->tag->passwordField("senha") ?>
 </p>

 <p>
    <label for="senha2">Repita sua Senha</label>
    <?php echo $this->tag->passwordField("senha2") ?>
 </p>

 <p>
    <?php echo $this->tag->submitButton("Próximo") ?>
 </p>

</form>