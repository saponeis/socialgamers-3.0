var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    clean           = require('gulp-clean'),
    concat          = require('gulp-concat'),
    uglify          = require('gulp-uglify'),
    bower           = require('gulp-bower-files'),
    bytediff        = require('gulp-bytediff'),
    sourcemaps      = require('gulp-sourcemaps');

var pkg = {
    dev: {
        js: "./app/assets/js/**/*.js",
        sass: "./app/assets/sass/main.scss"
    },
    public: {
        js: "./public/js",
        css: "./public/css"
    }
};

gulp.task('js', ['clean-js'], function() {
    var source = pkg.dev.js;
    return gulp.src(source)
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.min.js', {newLine: ';'}))
        .pipe(bytediff.start())
        .pipe(uglify({mangle: true}))
        .pipe(bytediff.stop())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(pkg.public.js));
});

gulp.task('sass', ['clean-sass'], function () {
    var source = pkg.dev.sass;
    return gulp.src(source)
        .pipe(sourcemaps.init())
        .pipe(concat('styles.min.js'))
        .pipe(bytediff.start())
        .pipe(sass().on('error', sass.logError))
        .pipe(bytediff.stop())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(pkg.public.css));
});

gulp.task('clean-js', function () {
    return gulp.src(pkg.public.js, {read: false})
        .pipe(clean());
});

gulp.task('clean-sass', function () {
    return gulp.src(pkg.public.css, {read: false})
        .pipe(clean());
});

gulp.task('watch', function () {
    gulp.watch(pkg.dev.js, ['js']);
    gulp.watch(pkg.dev.sass, ['sass']);
});
gulp.task('build', ['js', 'sass']);
gulp.task('default', ['build', 'watch']);